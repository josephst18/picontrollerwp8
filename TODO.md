# TODO

1. App currently sends a POST request to my website's about page (josephstahl.com/about).  
This needs to be sent to a page hosted on RPi instead.  
2. Get RPi from Birmingham and set it up as web server.  Create pages (Rpi.lan/lightSwitch) that 
will execute a script that toggles voltage at a GPIO pin and toggles the state of a high voltage switch.
3. Change behavior of webpage so that script returns a value, which the website passes back on the POST
request, which the app then interprets as success or failure.
4. Add textbox to allow user to input address of RPi.  (Validate input)